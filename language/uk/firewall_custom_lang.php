<?php

$lang['firewall_custom_app_description'] = 'Додаток «Правила користувача» надає низькорівневий інструмент для налаштування розширених правил брандмауера.';
$lang['firewall_custom_app_name'] = 'Правила користувача';
$lang['firewall_custom_command_is_not_permitted'] = 'Команда заборонена.';
$lang['firewall_custom_description_invalid'] = 'Опис недійсний.';
$lang['firewall_custom_firewall_rule_invalid'] = 'Правило брандмауера недійсне.';
$lang['firewall_custom_help_dragging'] = 'Порядок ваших правил брандмауера важливий - клацніть і перетягніть правило вгору або вниз в таблиці, щоб змінити порядок.';
$lang['firewall_custom_help_iptables_constant'] = '$IPTABLES - це константа для команди iptables, яка включає прапори для запобігання проблемам із блокуванням файлів.';
$lang['firewall_custom_restart_required'] = 'Порядок ваших правил міг змінитися – потрібен перезапуск брандмауера.';
$lang['firewall_custom_summary'] = 'Дозволяє додавати правила брандмауера. Для розширеного налаштування правил брандмауера, яку не можна додати за допомогою доступних додатків для брандмауера (вихідний брандмауер , DMZ, Мульти-WAN, прокидання портів тощо). так само для досягнення бажаної безпеки та маршрутизації пакетів можна використовувати додаток «Правила користувача брандмауера». Використовувати цей модуль можуть лише досвідчені користувачі.';
$lang['firewall_custom_invalid_ip_version'] = 'IP version is invalid.';
$lang['firewall_custom_invalid_configuration_parameter'] = 'Неприпустимий параметр конфігурації.';
